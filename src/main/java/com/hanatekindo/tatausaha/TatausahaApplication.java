package com.hanatekindo.tatausaha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class TatausahaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TatausahaApplication.class, args);
	}

}
